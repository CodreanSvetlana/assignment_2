
const FIRST_NAME = "Svetlana";
const LAST_NAME = "Codrean";
const GRUPA = "1090";



function initCaching() {
    var cache = {
        home: 0,
        about: 0,
        contact: 0
    };

    cache.pageAccessCounter = function(section) {
        if(section == undefined)
            cache.home++;
        else if(section.toLowerCase() == "contact")
            cache.contact++;
        else if(section.toLowerCase() == "about")
            cache.about++;
    }
    cache.getCache = function() {
        return cache;
    }
    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

